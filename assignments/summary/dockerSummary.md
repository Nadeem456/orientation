**_Docker Basics._**
-------------------------------------------------------------------------------------------------------------
![db](https://miro.medium.com/max/3172/1*y6CvfE6WUgoIdT8Mp0Ev_g.png)  



 
**What is Docker?**  
-------------------------------------------------------------------------------------------------------------
 * Docker is a progam that helps developer to run the program in loosely isolated environment called container without problem of dependencies on user's machine's kernal.  
 * It helps to run many containers simultaneously,as virtual machine on host machine.

  

**Features of Docker.**  
-------  
 1. Fast, consistent delivery of your applications.  
 2. Responsive deployment and scaling.  
 3. Running more workloads on the same hardware.   


**Docker Image.**  
--------------
  ![di](https://geekflare.com/wp-content/uploads/2019/07/dockerfile-697x270.png)    
It contains everything required to run the code as a container.  
It can be deployed to any docker environment. 
It includes:   
1. code  
2. runtime  
3. libraries  
4. environment variables  
5. configuration files.  


**Docker Container.**  
--------------
 ![dc](https://miro.medium.com/max/2048/0*ujI404Gnomn1Wz5h.png)  

It is running docker image.  


**Docker Hub.**  
----------------  
![dh](https://solutionsanz.files.wordpress.com/2017/08/github-dockerhub1.png?w=1200)   

It acts like github for docker images and containers.  


**_Docker Commands._**  
-------------
  ![dc](https://image.slidesharecdn.com/docker-161208023933/95/from-vms-to-containers-introducing-docker-containers-for-linux-and-windows-server-13-638.jpg?cb=1481164794)

The above command can be used as:  

>$ docker start nadeemrza  
 
docker starts the conatainer nadeemrza.  

>$ docker rm nadeemrza  

docker deletes the container nadeemrza.   






**_Operations/Workflow in Docker._** 
---------------------
  ![dw](https://dzone.com/storage/temp/5288806-docker-stages.png)   

 1. _Download/pull_ the docker images that you want to work with.
  >docker pull nadeemrza/trydock

 2. Copy your code inside the docker.  
  >docker start e0b72ff850f8  
  >docker cp hello.py e0b72ff850f8:/  
  >docker exec e0b72ff850f8 python3 hello.py
  

say our program is simple python command:
>print("Hello, I am inside container")  

 3. Access docker terminal.

 4. Install additional required dependencies.
   * Give permission to run shell script
  >docker exec -it e0b72ff850f8 chmod +x requirements.s
   * Install dependencies
  >docker exec -it e0b72ff850f8 /bin/bash ./requirements.sh

 5. Compile and Run the Code inside docker  
>docker run -ti nadeemrza/trydock /bin/bash    

  which will give output as:  

>root@e0b72ff850f8:/#
  
  This confirms that you are running the docker container with conatiner ID _e0b72ff850f8_ and you are inside the container.

 6. Document steps to run your program in README.md file.

 7. Commit the changes done to the docker.
  >docker commit e0b72ff850f8 nadeemrza/trydock

 8. Push docker image to the docker-hub.
  >docker tag nadeemrza/trydock snehabhapkar/trial-dock-img

 9. share repository with people who want to try your code.
  >docker push snehabhapkar/trial-dock-img  



**_Docker Installation._**  
------------  

Follow the [given site] to know about the process to install the _Docker_ on _Ubuntu_.

[given site]:https://docs.docker.com/engine/install/ubuntu/

