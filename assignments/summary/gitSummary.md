**_Learn GitLab_**  
-------------------------------------------------------------------------------------------------------------

**What is _Gitlab_?**   
-------------------------------------------
![gl](https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSyGpy1_yWrgmE8n5DdUbSMCqK7YkoeAd_ydw&usqp=CAU)  


* GitLab is a complete DevOps platform delivered as a single application.
* Fundamentally changing the way Development, Security, and Ops teams collaborate and build software.
* GitLab helps teams   
   * improve cycle time from weeks to minutes
   * reduce development costs and time to market while increasing developer productivity.
* Git lets you easily keep track of every revision you and your team make during the development of your software.        
  
  
  

**_Git Terms._** 
-------------------------------------------------------------------------------------------------------------
![gt](https://i.redd.it/nm1w0gnf2zh11.png)  
1. **Repository:**  
It is collection of codes as a box,where your team throw code into and helps you keep a track of them.

2. **Commit:**  
As the name indicates it commit the changes at that instant.

3. **Push:**  
It means pushing your changes to gitlab.

4. **Branch:**  
From the name itself we say it is branch of the main or master code which can be merged together once code is error free.A master code can have many branches.

5. **Merge:**  
It is process of merging or joining all the branches of code to master code.

6. **Clone:**  
It is process of making a copy of code from online repository to local machine.

7. **Fork:**  
It is like cloning but makes a new fresh repo of that code under you own name.   
  
  
  

**Git Tree.**  
-----------------------------------------------------------------------------------------------------------  
1. **Workspace:**  
changes in tree via editors(gedit, notepad, vim, nano) is done in this tree.
2. **Staging:**  
It is the stage when the git starts tracking the changes that are made in the file.
3. **Local Repository:**  
files that are comitted go into in this repository.
4. **Remote Repository:**  
All the local repository which are _pushed_ are saved in this repository.it is exact copy of local repository but it is saved on sever.  
  
  
  
  

**_Getting Started With Gitlab._**  
------------------------------------------------------------------------------------------------------------ 

**Installing Gitlab:**  
-------------------------------------------------------------------------------------------------------------
For windows just go to [official site](https://about.gitlab.com/install/) and follow the steps of installation.

for linux too we have commands on [above site](https://about.gitlab.com/install/)  
or use the command:    
>$ sudo apt-get install git  
  
(It particulary works for Ubuntu)  
  
  
  
  
  

**Git File Stages:**  ------------------------------------------------------------------------------------------------------------ 
![gs](https://backlog.com/app/themes/backlog-child/assets/img/guides/git/basics/git_workflow_002.png)

1. **Modify:**  
means that you have changed the file but have not committed it to your repo yet.

2. **Stage:**  
means that you have marked a modified file in its current version to go into your next picture/snapshot.

3. **Commit:**  
means that the data is safely stored in your local repo in form of pictures/snapshots.        
  
  
  
  
  

**_Git Workflow Syntax._**  
-------------------------------------------------------------------------------------------------------------

* _Clone_ the repo:

>$ git clone <link-to-repository>

* _Create_ a new branch:

>$ git checkout master
>
$ git checkout -b <your-branch-name>



* _Add_ changes to the staging area;

>$ git add

* _Commit_:

>$ git commit -sv

* _Push_:

>$ git push origin <branch-name>


